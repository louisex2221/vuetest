/**
 * 等待
 */
export const sleep = function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
/**
 * 存儲localStorage
 */
export const setLocalStorage = (name, content) => {
  let keyName = process.env.HTML_TITLE.replaceAll('"', "");
  let localName = keyName + "_" + name;
  if (!localName) return;
  if (typeof content !== "string") {
    content = JSON.stringify(content);
  }
  window.localStorage.setItem(localName, content);
};

/**
 * 獲取localStorage
 */
export const getLocalStorage = (name) => {
  let keyName = process.env.HTML_TITLE.replaceAll('"', "");
  let localName = keyName + "_" + name;
  if (!localName) return;
  let originVal = window.localStorage.getItem(localName);
  try {
    let translateText = JSON.parse(originVal);
    return translateText;
  } catch (error) {
    return originVal;
  }
};

/**
 * 刪除localStorage
 */
export const removeLocalStorage = (name) => {
  let keyName = process.env.HTML_TITLE.replaceAll('"', "");
  let localName = keyName + "_" + name;
  if (!localName) return;
  window.localStorage.removeItem(localName);
};
export const removeAllLocalStorage = () => {
  let keyName = process.env.HTML_TITLE.replaceAll('"', "");
  let noRemove = ["loginData"];
  Object.keys(localStorage).filter((item) => {
    if (item.indexOf(keyName) !== -1) {
      noRemove.map((removeItem) => {
        let localName = keyName + "_" + removeItem;
        if (item === localName) {
          return;
        } else {
          window.localStorage.removeItem(item);
        }
      });
    }
  });
};

// time

// 補小數點後兩位
export const moneyFormat = (item) => {
  let num = 0;
  if (item == 0) {
    return num;
  } else {
    num = parseFloat(item).toFixed(2);
    return num;
  }
};
// 補+-號
export const plusmnFormat = (num) => {
  if (num.toString().indexOf("-") == -1 && num.toString() !== "0") {
    return "+" + num;
  } else {
    return num;
  }
};

export const judgeBrand = (sUserAgent) => {
  var isIphone = sUserAgent.match(/iphone/i) == "iphone";
  var isHuawei = sUserAgent.match(/huawei/i) == "huawei";
  var isHonor = sUserAgent.match(/honor/i) == "honor";
  var isOppo = sUserAgent.match(/oppo/i) == "oppo";
  var isOppoR15 = sUserAgent.match(/pacm00/i) == "pacm00";
  var isVivo = sUserAgent.match(/vivo/i) == "vivo";
  var isXiaomi = sUserAgent.match(/mi\s/i) == "mi ";
  var isXiaomi2s = sUserAgent.match(/mix\s/i) == "mix ";
  var isRedmi = sUserAgent.match(/redmi/i) == "redmi";
  var isSamsung = sUserAgent.match(/sm-/i) == "sm-";

  if (isIphone) {
    return "iphone";
  } else if (isHuawei || isHonor) {
    return "huawei";
  } else if (isOppo || isOppoR15) {
    return "oppo";
  } else if (isVivo) {
    return "vivo";
  } else if (isXiaomi || isRedmi || isXiaomi2s) {
    return "xiaomi";
  } else if (isSamsung) {
    return "samsung";
  } else {
    return "default";
  }
};
// 碰撞偵測
export const hitHandler = function hitHandler(r1, r2) {
  //Define the variables we'll need to calculate
  let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
  //hit will determine whether there's a collision
  hit = false;

  //Find the center points of each sprite
  r1.centerX = r1.x + r1.width / 2;
  r1.centerY = r1.y + r1.height / 2;
  r2.centerX = r2.x + r2.width / 2;
  r2.centerY = r2.y + r2.height / 2;

  //Find the half-widths and half-heights of each sprite
  r1.halfWidth = r1.width / 2;
  r1.halfHeight = r1.height / 2;
  r2.halfWidth = r2.width / 2;
  r2.halfHeight = r2.height / 2;

  //Calculate the distance vector between the sprites
  vx = r1.centerX - r2.centerX;
  vy = r1.centerY - r2.centerY;

  //Figure out the combined half-widths and half-heights
  combinedHalfWidths = r1.halfWidth + r2.halfWidth;
  combinedHalfHeights = r1.halfHeight + r2.halfHeight;
  //Check for a collision on the x axis
  if (Math.abs(vx) < combinedHalfWidths) {
    //A collision might be occuring. Check for a collision on the y axis
    if (Math.abs(vy) < combinedHalfHeights) {
      //There's definitely a collision happening
      hit = true;
    } else {
      //There's no collision on the y axis
      hit = false;
    }
  } else {
    //There's no collision on the x axis
    hit = false;
  }
  //`hit` will be either `true` or `false`
  return hit;
};

// 亂數
export const getRandom = function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};
