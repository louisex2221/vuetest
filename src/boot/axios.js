import { boot } from "quasar/wrappers";
import { getLocalStorage } from "@utils/method.js";
import { swal } from "boot/swalert";
import { i18n } from "boot/i18n";
import axios from "axios";
const apiUrl = process.env.DEV_API_URL.replaceAll('"', "");
const api = axios.create({ baseURL: apiUrl });
export default boot(({ app, router }) => {
  api.interceptors.request.use(
    (config) => {
      config.headers["Authorization"] = `Bearer ${getLocalStorage(
        "access_token"
      )}`;
      return config;
    },
    (error) => {
      // Do something with request error
      Promise.reject(error);
    }
  );
  // response interceptor
  api.interceptors.response.use(
    async (response) => {
      return response;
    },
    async (error) => {
      let errData = error.response;
      switch (errData.status) {
        case 401:
          errData.codeMsg = "[401]  " + errData.data.message;
          let swlRes = await swal.fire({
            icon: "warning",
            text: errData.codeMsg,
          });
          if (swlRes.value) {
            router.push({ path: "/login" });
          }
          break;
      }
      return error;
    }
  );
  app.config.globalProperties.$axios = axios;

  app.config.globalProperties.$api = api;
});

export { axios, api };
