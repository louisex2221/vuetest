/** @format */

/** @format */
import { boot } from "quasar/wrappers";
import { defineRule } from "vee-validate";
import { i18n } from "boot/i18n";
export default boot(({ app }) => {
  defineRule("required", (value) => {
    if (!value || !value.length) {
      return i18n.global.tc("validate.required");
    }
    return true;
  });
  defineRule("email", (value) => {
    if (!value || !value.length) {
      return true;
    }
    if (!/[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/.test(value)) {
      return "This field must be a valid email";
    }
    return true;
  });
  defineRule("min", (value, [min, max]) => {
    if (!value || !value.length) {
      return true;
    }
    const numericValue = Number(value);
    if (numericValue < min) {
      return `This field must be greater than ${min}`;
    }
    if (numericValue > max) {
      return `This field must be less than ${max}`;
    }
    return true;
  });
});
