import { ref, reactive } from "vue";
import { createNamespacedHelpers } from "vuex-composition-helpers";
import { useI18n } from "vue-i18n";
import { swal2, swal3 } from "boot/swalert";
export default function (moduleName) {
  const { t } = useI18n();
  const { useActions } = createNamespacedHelpers(moduleName);
  const {
    actionGetDataList,
    actionGetInfoData,
    actionEditData,
    actionAddData,
    actionDeleteData,
  } = useActions([
    "actionGetDataList",
    "actionGetInfoData",
    "actionEditData",
    "actionAddData",
    "actionDeleteData",
  ]);
  const hook_dataList = ref([]);
  const hook_pagination = reactive({
    page: 1,
    per_page: 5,
    total: 1,
  });
  const hook_addCfg = reactive({
    flag: false,
  });
  const hook_editCfg = reactive({
    flag: false,
    id: null,
  });
  const hook_permissions = ref();
  // 獲取資料
  async function hook_funcGeList(postData) {
    const params = {
      page: hook_pagination.page,
      per_page: hook_pagination.per_page,
      postData: postData,
    };
    const resData = await actionGetDataList(params);

    if (resData.code == 200) {
      let res = resData.data;
      hook_dataList.value = res.lists;
      hook_pagination.total = res.total;
    }
  }
  // 編輯開關
  async function hook_funcChangeEditCfg(id) {
    hook_editCfg.flag = !hook_editCfg.flag;
    hook_editCfg.id = id;
  }
  // 獲得單筆資料
  async function hook_funcGetInfo() {
    let postData = {
      id: hook_editCfg.id,
    };
    let res = await actionGetInfoData(postData);
    return res;
  }
  // 編輯資料
  async function hook_funcEditInfo(postData) {
    let res = await actionEditData(postData);
    if (res.code === 200) {
      hook_funcChangeEditCfg(null);
      swal3.fire({
        icon: "success",
        text: t("system.editSuc"),
      });
    } else {
      swal3.fire({
        icon: "warning",
        text: res.message,
      });
    }
    return res;
  }
  //新增開關
  function hook_funcChangeAddCfg() {
    hook_addCfg.flag = !hook_addCfg.flag;
  }
  // 新增資料
  async function hook_funcAddInfo(postData) {
    let res = await actionAddData(postData);
    if (res.code === 200) {
      hook_funcChangeAddCfg(null);
      swal3.fire({
        icon: "success",
        text: t("system.addSuc"),
      });
    } else {
      swal3.fire({
        icon: "warning",
        text: res.message,
      });
    }
    return res;
  }
  // 刪除資料
  async function hook_funcChangeDelInfo(id) {
    let swlRes = await swal2.fire({
      icon: "question",
      text: t("system.deleteTitle"),
    });
    if (swlRes.value) {
      let res = await actionDeleteData({ id });
      if (res.code === 200) {
        swal3.fire({
          icon: "success",
          text: t("system.delSuc"),
        });
      } else {
        swal3.fire({
          icon: "warning",
          text: res.message,
        });
      }
      return res;
    }
  }
  // 換頁
  async function hook_funcChangePagination({ page, per_page, total }) {
    hook_pagination.page = page ? page : hook_pagination.page;
    hook_pagination.per_page = per_page ? per_page : hook_pagination.per_page;
    hook_pagination.total = total ? total : hook_pagination.total;
  }

  return {
    //hook confing
    hook_dataList,
    hook_pagination,
    hook_addCfg,
    hook_editCfg,
    hook_permissions,
    //修改
    hook_funcChangeEditCfg,
    hook_funcGeList,
    hook_funcGetInfo,
    hook_funcEditInfo,
    //新增
    hook_funcChangeAddCfg,
    hook_funcAddInfo,
    //刪除
    hook_funcChangeDelInfo,
    //切換Page
    hook_funcChangePagination,
    // 獲得權限
  };
}
