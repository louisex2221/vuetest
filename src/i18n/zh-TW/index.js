/** @format */

import system from "./system";
import menu from "./menu";
import lang from "./lang";
import authUser from "./authUser";
import authRole from "./authRole";
import question from "./question";
import label from "./label";
import validate from "./validate";
export default {
  system,
  menu,
  lang,
  authUser,
  authRole,
  question,
  label,
  validate,
};
