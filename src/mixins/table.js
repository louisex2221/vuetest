import Swal from "sweetalert2";
export default {
  data() {
    return {
      mixi_dataList: [],
      // Alert
      mixi_alertDialogFlag: false,
      // Add
      mixi_addDialogFlag: false,
      // Edit
      mixi_editDialogFlag: false,
      mixi_editData: {},
      // Pagination
      mixi_page: 1, // 頁數
      mixi_perpage: 10, // 預設幾筆
      mixi_total: 0,
    };
  },
  methods: {
    async mixi_pageListRefresh() {
      const postData = {
        page: this.mixi_page, // 頁數
        perpage: this.mixi_perpage, // 預設幾筆
        ...this.filterList,
      };
      const resData = await this.actionGetDataList(postData);
      if (resData.code == 200) {
        let res = resData.data;
        this.mixi_dataList = res.list;
        this.mixi_total = res.total;
      }
    },
    mixi_changeAlertDialog() {
      this.mixi_alertDialogFlag = !this.mixi_alertDialogFlag;
    },
    mixi_changeAddDialog() {
      this.mixi_addDialogFlag = !this.mixi_addDialogFlag;
    },
    mixi_changeEditDialog(editData) {
      this.mixi_editDialogFlag = !this.mixi_editDialogFlag;
      this.mixi_editData = this.mixi_editDialogFlag ? editData : {};
    },
    mixi_changePagination(pageData) {
      this.mixi_page = pageData.page;
      this.mixi_perpage = pageData.perpage;
      this.mixi_pageListRefresh();
    },
    async mixi_deleteData(deleteId) {
      const res = await Swal.fire({
        title: this.$t("system.deleteTitle"),
        text: this.$t("system.deleteAlert"),
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#21BA45",
        cancelButtonColor: "#C10015",
        allowOutsideClick: false,
        cancelButtonText: this.$t("system.cancel"),
        confirmButtonText: this.$t("system.sure"),
      });
      if (res.value) {
        const postData = {
          id: deleteId,
        };
        await this.actionDeleteData(postData);
        await Swal.fire({
          title: this.$t("system.prompt"),
          text: this.$t("system.deleted"),
          icon: "success",
        });
        await this.mixi_pageListRefresh();
      }
    },
  },
};
