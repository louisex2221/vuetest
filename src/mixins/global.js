/** @format */
import { defineComponent } from "vue";
import { createNamespacedHelpers } from "vuex";
const { mapGetters, mapActions } = createNamespacedHelpers("crmGlobal");
export default defineComponent({
  computed: {
    ...mapGetters({
      globalUser: "getUserData", // user資料
      globalSystem: "getSystem", // 系統開關
    }),
  },
  methods: {
    ...mapActions(["globalAcChangeNavFlag"]),
  },
});
