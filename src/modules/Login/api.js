/** @format */
import { api } from "boot/axios";

export function apiLogin(postData) {
  return api({
    url: "/login",
    data: postData,
    method: "POST",
  });
}
