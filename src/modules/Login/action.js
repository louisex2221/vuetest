/** @format */
import { apiLogin } from "./api";
export const actions = {
  async actionLogin({ commit }, postData) {
    try {
      let resData = await apiLogin(postData);
      return resData.data;
    } catch (error) {
      return error;
    }
  },
};
