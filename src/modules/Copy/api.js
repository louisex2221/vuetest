/** @format */

import { api } from "boot/axios";
export function apiGetDataList(postData) {
  return api({
    url: "/admin/group/list",
    method: "GET",
    params: postData,
  });
}

export function apiGetInfoData(postData) {
  return api({
    url: "/admin/group/detail/" + postData.id,
    method: "GET",
  });
}

export function apiAddData(postData) {
  return api({
    url: "/admin/group/store",
    method: "POST",
    data: postData,
  });
}

export function apiEditData(postData) {
  return api({
    url: "/admin/group/update/" + postData.id,
    method: "PUT",
    data: postData,
  });
}

export function apiDeleteData(postData) {
  return api({
    url: "/admin/group/delete/" + postData.id,
    method: "DELETE",
  });
}
